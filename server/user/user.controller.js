const { User } = require('./user.model');

// Login User
exports.login = async (req, res) => {
  console.log('Login');
  const existingUser = await User.findOne({ email: req.body.email }).select({ '__v': 0});
  console.log('User: ', existingUser);
  if (existingUser.password ===  req.body.password) {
    res.send({
      result: existingUser,
      status: 'SUCCESS',
      message: 'Successfully logged In.'
    })
  } else {
    res.send({
      status: 'ERROR',
      message: 'Please check Email and Password.'
    })
  }
}

// Registration
exports.register = async (req, res) => {
  try {
    const body = req.body;
    if (body.email && body.password){
      const newUser = new User(req.body);
      const result = await newUser.save()
      res.send({
        result: result,
        status: 'SUCCESS',
        message: 'Successfully Created.'
      });
    } else {
      res.send({
        status: 'ERROR',
        message: 'Please add email and password in request body.'
      });
    }
  } catch(error) {
    res.send({
      status: 'ERROR',
      message: 'Duplicate Email ID is present.'
    });
  }
}