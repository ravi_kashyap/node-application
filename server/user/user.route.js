const User = require('./user.controller');
var express = require('express');
var router = express.Router();

router.get('/login', (req, res) => {
  res.render('login', {
    title: 'Welcome to Login Page'
  });
});

router.post('/login', User.login);

router.post('/register', User.register);

module.exports = router;
