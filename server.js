const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const expressSession = require('express-session');

require('./config/db.js');
require('./server/user/user.route');

const app = express();

const port = process.env.PORT || 3000;

// Template Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// Middleware
app.use(
  expressSession({
  secret: 'top Secret',
  resave: true,
  saveUninitialized: false
}));

app.use(bodyParser.json());

app.use(express.static(__dirname + '/views'));

app.use('/', require('./server/user/user.route'));

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.send(error.message);
});

// Server start at port no 3000
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`)
})
