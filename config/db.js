const mongoose = require('mongoose');

mongoose.set('useCreateIndex', true);

mongoose.connect('mongodb://localhost:27017/Application', { useNewUrlParser: true, useUnifiedTopology: true, }, (error) => {
  if (!error) {
    console.log('sucessfully connected to MongoDB');
  } else {
    console.log('error in connecting');
  }
});

